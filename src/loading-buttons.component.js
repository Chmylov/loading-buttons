class LoadingButtons {
  constructor() {
    this.buttons = document.querySelectorAll(
      '[component="loading-button"], [component="loading-buttons"]'
    );
    this.enrichButtons();
    this.setStyle();
  }

  setStyle() {
    let style = document.createElement('style');
    style.innerHTML =
      '[component=loading-button] .spinner,[component=loading-buttons] .spinner{width:30px;height:30px;position:relative;margin:auto;}[component=loading-button] .spinner div,[component=loading-buttons] .spinner div{position:absolute;width:inherit;height:inherit;border-radius:100%;border:5px solid;border-color:white transparent transparent transparent;animation:rotate 1s infinite ease-in-out}[component=loading-button] .spinner div:last-child,[component=loading-buttons] .spinner div:last-child{animation-delay:-0.15s}@keyframes rotate{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}';
    let ref = document.querySelector('link');
    ref.parentNode.insertBefore(style, ref);
  }

  enrichButtons() {
    for (let button of this.buttons) {
      button.addEventListener('click', event =>
        LoadingButtons.clickHandler(event)
      );

      button.loading = {
        cachedInnerHTML: button.innerHTML,
        cachedStyleAttribute: button.getAttribute('style') || '',
        offsetHeight: button.offsetHeight,
        offsetWidth: button.offsetWidth
      };
    }
  }

  stopAll() {
    for (let button of this.buttons) {
      LoadingButtons.hideSpinner(button);
    }
  }

  stop(selector) {
    let buttons = document.querySelectorAll(selector);

    for (let button of buttons) {
      if (button.loading && button.disabled) {
        LoadingButtons.hideSpinner(button);
        break;
      }
    }
  }

  static getSpinnerHtml() {
    return '<div class="spinner"><div></div><div></div></div>';
  }

  static showSpinner(button) {
    button.innerHTML = LoadingButtons.getSpinnerHtml();
    button.style.height = button.loading.offsetHeight + 'px';
    button.style.width = button.loading.offsetWidth + 'px';
    button.style.padding = '0';
    button.setAttribute('disabled', '');
  }

  static hideSpinner(button) {
    button.innerHTML = button.loading.cachedInnerHTML;

    if (button.loading.cachedStyleAttribute) {
      button.setAttribute('style', button.loading.cachedStyleAttribute);
    }

    button.removeAttribute('disabled');
  }

  static clickHandler(event) {
    let clickedButton = event.currentTarget;
    if (!clickedButton.disabled) {
      LoadingButtons.showSpinner(clickedButton);
    }
  }
}

Window.prototype.loadingButtons = new LoadingButtons();
