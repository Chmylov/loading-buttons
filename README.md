# Loading button component
Loading button with spinner is created to give user feedback on his click action

## Usage
Include ``loading-buttons.component.js`` from dist folder to your project. If you are using latest javaScript you can use ``loading-buttons.component.js`` from src folder.
 
Add ``component="loading-button`` to active the loading button.

#### Methods:
Stop specific loading button: ``loadingButton.stop('Seletor')``  
Stop all  loading buttons  ``loadingButton.stopAll()``