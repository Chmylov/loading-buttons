"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var LoadingButtons =
/*#__PURE__*/
function () {
  function LoadingButtons() {
    _classCallCheck(this, LoadingButtons);

    this.buttons = document.querySelectorAll('[component="loading-button"], [component="loading-buttons"]');
    this.enrichButtons();
    this.setStyle();
  }

  _createClass(LoadingButtons, [{
    key: "setStyle",
    value: function setStyle() {
      var style = document.createElement('style');
      style.innerHTML = '[component=loading-button] .spinner,[component=loading-buttons] .spinner{width:30px;height:30px;position:relative;margin:auto;}[component=loading-button] .spinner div,[component=loading-buttons] .spinner div{position:absolute;width:inherit;height:inherit;border-radius:100%;border:5px solid;border-color:white transparent transparent transparent;animation:rotate 1s infinite ease-in-out}[component=loading-button] .spinner div:last-child,[component=loading-buttons] .spinner div:last-child{animation-delay:-0.15s}@keyframes rotate{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}';
      var ref = document.querySelector('link');
      ref.parentNode.insertBefore(style, ref);
    }
  }, {
    key: "enrichButtons",
    value: function enrichButtons() {
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.buttons[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var button = _step.value;
          button.addEventListener('click', function (event) {
            return LoadingButtons.clickHandler(event);
          });
          button.loading = {
            cachedInnerHTML: button.innerHTML,
            cachedStyleAttribute: button.getAttribute('style') || '',
            offsetHeight: button.offsetHeight,
            offsetWidth: button.offsetWidth
          };
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: "stopAll",
    value: function stopAll() {
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = this.buttons[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var button = _step2.value;
          LoadingButtons.hideSpinner(button);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
            _iterator2["return"]();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    }
  }, {
    key: "stop",
    value: function stop(selector) {
      var buttons = document.querySelectorAll(selector);
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = buttons[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var button = _step3.value;

          if (button.loading && button.disabled) {
            LoadingButtons.hideSpinner(button);
            break;
          }
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
            _iterator3["return"]();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }
    }
  }], [{
    key: "getSpinnerHtml",
    value: function getSpinnerHtml() {
      return '<div class="spinner"><div></div><div></div></div>';
    }
  }, {
    key: "showSpinner",
    value: function showSpinner(button) {
      button.innerHTML = LoadingButtons.getSpinnerHtml();
      button.style.height = button.loading.offsetHeight + 'px';
      button.style.width = button.loading.offsetWidth + 'px';
      button.style.padding = '0';
      button.setAttribute('disabled', '');
    }
  }, {
    key: "hideSpinner",
    value: function hideSpinner(button) {
      button.innerHTML = button.loading.cachedInnerHTML;

      if (button.loading.cachedStyleAttribute) {
        button.setAttribute('style', button.loading.cachedStyleAttribute);
      }

      button.removeAttribute('disabled');
    }
  }, {
    key: "clickHandler",
    value: function clickHandler(event) {
      var clickedButton = event.currentTarget;

      if (!clickedButton.disabled) {
        LoadingButtons.showSpinner(clickedButton);
      }
    }
  }]);

  return LoadingButtons;
}();

Window.prototype.loadingButtons = new LoadingButtons();
